define(function(require) {
  var $ = require('jquery');
  var MagicConstant = require('MagicConstant');
  var CDNLIB = MagicConstant.CDNLIB;
  var magicConfig = require('../MagicConfig');
  var url = magicConfig.magicBase;
  var index=1,           //当前亮区位置
  // prevIndex=14,          //前一位置
  prevIndex=12,          //前一位置
  Speed=300,           //初始速度
  Time,            //定义对象
  // arr_length = 14; //GetSide(5,5),         //初始化数组
  arr_length = 12; //GetSide(5,5),         //初始化数组
  EndIndex=1,           //决定在哪一格变慢
  cycle=0,           //转动圈数   
  EndCycle=3,           //计算圈数
  flag=false,           //结束转动标志
  random_num=1,      //中奖数
  quick=0;           //加速

  seajs.use(CDNLIB+'/jquery.ba-cond.min.js');  
   
  if(window.jQuery) {
    init();
  }
  
  
function init() {  
  $(function() {
    
/*
    $('#global-header-section').load('_header.html',function(){
        seajs.use('page/_header.js');
        $('#go-download-section').click(function(event) {
          event.preventDefault();
          $navs.eq(4).click();
        })
    });
*/
  
// 首页的footer暂时搞成静态的  
/*
    $('#global-footer-section').load('_footer.html',function(){
      seajs.use('page/_footer.js');
    });
*/
    seajs.use('page/_header.js');
    $('#go-download-section').click(function(event) {
      event.preventDefault();
      $navs.eq(4).click();
    })
    seajs.use('page/_footer.js');
    seajs.use(CDNLIB+'/modernizr.custom.92426.js', function() {
      Modernizr.load({
        test: Modernizr.csstransitions && Modernizr.csstransforms3d,
        yep: [CDNLIB+'/jquery.slitslider.js', 'js/page/custom-slitslider.js'],
        nope: ['js/page/crossfade.js']
      })
      
      // 触摸设备控制
      Modernizr.load({
        test: Modernizr.touch,
        yep: [CDNLIB+'/jquery.touchSwipe.min.js'],
        complete: function() {
        if(!Modernizr.touch) return;
          $('#main').swipe({
          swipe:function(event, direction, distance, duration, fingerCount) {
            if(direction == 'up') {
              scroll('bt');
            } else if (direction == 'down') {
              scroll('tb');
            }
          }
        });
        }
      })
    });

    $('.section3 ul li').hover(function() {
      $(this).toggleClass('hover');
    })
    
    // Tab
    $('.tab-nav a').click(function() {
      var $tabContainer = $(this).closest('.tab-container');
      var $tabPanel = $tabContainer.find('.tab-panel');
      var $tabButton = $(this).closest('.tab-nav').find('a').removeClass('current');
      $(this).addClass('current');
      $tabPanel.hide().eq($(this).closest('li').index()).show();
      return false;
    })
    $('.tab-nav li:first-child a').click();
    
    /* 旋转木马 */
    function carousel(elem) {
      setInterval(function() {
        var $first = $(elem).find('li:first-child');
        $first.animate({marginTop: -$first.outerHeight(true)}, 1000, function() {
          $(this).css({marginTop: 0}).appendTo(elem);
        })
      }, 3000)
    }
    
    carousel('#home-news-list ul');
    
    // one page scroll
    var $main = $('#main'),
        $sections = $('#main > .fullscreen'),
        $navs = $('#section-nav li'),
        active = 0,
        total = $sections.length;
    $navs.eq(active).addClass('active');    
    $sections.not(':first').hide();
    $sections.eq(active).addClass('active');
    
    var animEndEventNames = {
      'WebkitAnimation' : 'webkitAnimationEnd',// Saf 6, Android Browser
      'MozAnimation'    : 'animationnend',      // only for FF < 15
      'animation'       : 'animationend'       // IE10, Opera, Chrome, FF 15+, Saf 7+
    }
  
    function scroll(dir) {
      var winHeight = $(window).height();
      var oldActive = active;
      active = (dir == 'bt') ? active + 1 : active - 1;
      if(active == total) {
        active = total - 1;
        return false;
      }
      if(active < 0) {
        active = 0;
        return false;
      }
      
      $sections.removeClass('active');
      var $activeSection = $sections.eq(active).addClass('active'),
          top = (dir == 'bt') ? winHeight : -winHeight;
      
      $navs.removeClass('active').eq(active).addClass('active');
      
/*
      if(Modernizr.csstransforms3d && Modernizr.csstransitions) {
        var dirClass = (dir == 'bt') ? 'section-up' : 'section-down';
        $activeSection.css({top: '0'}).show().addClass(dirClass);
        $sections.eq(oldActive).css({top: -top}).on(animEndEventNames[Modernizr.prefixed('animation')], function() {
          $(this).hide();
        })
      }
*/
      
      $activeSection.css({top: top}).show().stop().animate({
        top: 0
      }, 1000);
      $sections.eq(oldActive).animate({
        top: -top
      }, 1000, function() {
        $(this).hide();
      });
    }
    
    function fade(clicked) {
      var oldActive = active;
      active = clicked;
      $navs.removeClass('active');
      $navs.eq(active).addClass('active');
      $sections.removeClass('active');
      $sections.eq(active).addClass('active');
      $sections.eq(oldActive).fadeOut(300, function() {
        $sections.eq(active).css({top: '0'}).delay(50).fadeIn(300);
      });
    }
    
    // 点导航控制
    $navs.click(function() {
      var clicked = $navs.index(this);
      var oldActive = active;
      if($sections.is(':animated')) return false;
      if(clicked == active) return false;
      else if(clicked == active + 1) scroll('bt');
      else if(clicked == active - 1) scroll('tb');
      else {
        fade(clicked);
      }
    })
    
    // 鼠标滚轮控制
    seajs.use(CDNLIB+'/jquery.mousewheel.js', function() {
      $(document).mousewheel(function(event, delta) {
        if($sections.is(':animated')) {
          return false;
        }
        if(delta > 0) {
          scroll('tb')
        } else {
          scroll('bt')
        }
      })
    }); 
    
    // 方向键控制
    $(document).keydown(function(event) {
      if($sections.is(':animated')) {
        return false;
      }
      switch(event.keyCode) {
        case 38:
          scroll('tb');
          break;
        case 40:
          scroll('bt');
          break;
        case 32:
          if(event.shiftKey) {
            scroll('tb');
          } else {
            scroll('bt');
          }
          break;
      }    
    })
    
    $('#back-to-top').click(function(event) {
      event.preventDefault();
      $navs.eq(0).click();
    })
    
    // app下载链接跳转
    // if(location.hash == '#download-section') {
    //   $navs.eq(4).click();
    // }
    
    
    
    $('.dismissble-section .close').on('click', function() {
      $(this).closest('.dismissble-section').fadeOut(200, function() {
        $(this).remove();
      })
    })
    
/*
    // 第一屏上方的通知
    setTimeout(function() {
      $('#home-announce').fadeOut(400, function() {
        $(this).remove();
      })
    }, 15000);
    
    $('#home-announce .close').on('click', function() {
      $(this).closest('#home-announce').fadeOut(400, function() {
        $(this).remove();
      });
    })    
    })
    
*/
    
    
  });
} 

$(function(){
    
    function StartGame(){
    $("#random_box li").removeClass("random_current"); //取消选中
       //random_num = parseInt($("#txtnum").val());//
       // random_num = Math.floor(Math.random()*13+2); //产出随机中奖数2--12之间
       random_num = Math.floor(Math.random()*11+2); //产出随机中奖数2--12之间
       index=1; //再来一次,从1开始
       cycle=0;
       flag=false;
       //EndIndex=Math.floor(Math.random()*12);
       if(random_num>5) {
      EndIndex = random_num - 5; //前5格开始变慢
       } else {
      // EndIndex = random_num + 14 - 5; //前5格开始变慢
      EndIndex = random_num + 12 - 5; //前5格开始变慢
       }
       //EndCycle=Math.floor(Math.random()*3);
       Time = setInterval(Star,Speed);

    }
    function Star(num){
        //跑马灯变速
        if(flag==false){
          //走五格开始加速
          if(quick==5){
             clearInterval(Time);
             Speed=50;
             Time=setInterval(Star,Speed);
          }
          //跑N圈减速
          if(cycle==EndCycle+1 && index-1==EndIndex){
          clearInterval(Time);
             Speed=300;
             flag=true;         //触发结束
             Time=setInterval(Star,Speed);
          }
        }

        if(index>arr_length){
            index=1;
            cycle++;
        }

       //结束转动并选中号码
       if(flag==true && index==parseInt(random_num)){ 
          quick=0;
          clearInterval(Time);
       }
       $("#random_"+index).addClass('random_current'); //设置当前选中样式
       if(index>1)
           prevIndex=index-1;
       else{
           prevIndex=arr_length;
       }
       $("#random_"+prevIndex).removeClass('random_current'); //取消上次选择样式 
       index++;
       quick++;
    }

    

    
    // StartGame();点击抽奖按钮，开始抽奖;获取抽奖结果并展示。
    var onceClick = null;
    var onceShow = null;
    function luckDrawControll(){
      clearInterval(onceClick);
      clearInterval(onceShow);
      $("#start").removeClass('febb');
      $.ajax({
          cache: false,
          type: "post",
          // url: "http://192.168.50.73:8080/order"+"/h/v2/prize/luckDrawCount",
          url: url+"/h/v2/prize/luckDrawCount",
          data: {}, 
          success: function(obj) {
            var data = eval('(' + obj + ')');
            var prizeCount = data.prizeCount;
            if(prizeCount > 0){
              $("#start").on("click",function(){
                StartGame();
                $('#start').unbind("click");
                $("#start").addClass('febb');
                $.ajax({
                  cache: false,
                  type: "post",
                  url: url+"/h/v2/prize/luckDraw",
                  // url: "http://192.168.50.73:8080/order"+"/h/v2/prize/luckDraw",
                  data: {}, 
                  success: function(obj) {
                    var data = eval('(' + obj + ')');
                    var resultCode = data.resultCode;
                    changeResultContent(resultCode);
                    onceShow = setTimeout(function(){$('#lottery-result').show()},7000);
                    $("#lottery_query").on("click", function(event) {
                      event.preventDefault();
                      $('#lottery-result').show();
                    });
                    
                    $.ajax({
                      cache: false,
                      type: "post",
                      url: url+"/h/v2/prize/luckDrawCount",
                      // url: "http://192.168.50.73:8080/order"+"/h/v2/prize/luckDrawCount",
                      data: {}, 
                      success: function(obj) {
                        var data = eval('(' + obj + ')');
                        var prizeCount = data.prizeCount ;
                        $('#chanceNum').empty();
                        $('#chanceNum').append(prizeCount);
                        $('#start').addClass("febb");
                        $('#start').unbind("click");
                        if(prizeCount != 0){
                          onceClick = setInterval(luckDrawControll,7000);
                        }

                      }
                    });

                  }
                });

                
              }) 
            }else{
              $('#start').addClass("febb");
            }
          },
          error:function(){
            return false;
          }
      });
    }

    luckDrawControll();

    function changeResultContent(lotteryNum){
      var $contentTit = $("#content_tit").empty();
      var $contentText = $("#content_text").empty();
      var $contentEnd = $("#content_end").empty();
          $contentTit.removeClass();
          $contentText.removeClass();
          $contentEnd.removeClass();
      // var lotteryNum = 1;
      if(lotteryNum == 0){//未购买提示
        $contentTit.html('温馨提示');

        $contentText.addClass('hint');
        $contentText.html('您还没有抽奖资格,请购买手环再来抽奖哦。');
        $contentEnd.addClass('hint_butt');
        $contentEnd.html('确定');
      }else if(lotteryNum == 1){//未中奖
        $contentTit.html('很遗憾，未抽中!');
        
        $contentText.addClass('sorry');
        
        $contentEnd.html('感谢您的参与！请再接再厉！');
      }else if(lotteryNum == 3){//中红米Note F码
        $contentTit.html('恭喜您抽中红米Note F码！');
        
        $contentTit.addClass('hongMi_tit');
        $contentText.addClass('fCode');
        $contentText.html("<div class='lucky'></div>");
        $contentEnd.html('乐跑客服(电话：4000001257)会主动联系您');
      }else if(lotteryNum == 2){//中红米手机
        $contentTit.html('恭喜您抽中红米手机！');
         
        $contentTit.addClass('hongMi_tit');
        $contentText.addClass('hongMi');
        $contentText.html("<div class='lucky'></div>");
        $contentEnd.html('乐跑客服(电话：4000001257)会主动联系您');
      }
       
    }






  }) 
});