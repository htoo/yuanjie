define(function(require, exports, module) {
	// require('./Dialog.css');
	var $ = require('jquery'),
		Handlebars = require('handlebars'),
		EventBus = require('../event/EventBus'),
		dialogTpl = require('../view/CheckInvoiceDialog.html');
	// 或者通过 module.exports 提供整个接口

	module.exports = CheckInvoiceDialog;
	var me = {},
		$maskLayer;

	function CheckInvoiceDialog() {
		this.dialogTemplate = Handlebars.compile(dialogTpl);
		me = this;
	}


	//展示confirmDialog
	CheckInvoiceDialog.prototype.show = function(data) {
		var htmlStr = this.dialogTemplate(data);
		// alert(htmlStr);
		$maskLayer = $('<div class="mask_layer"></div>');
		$("body").append(htmlStr);
		var $dialog = $('.invoice-dialog');
		$dialog.before($maskLayer).css({
			marginTop: -$dialog.height() / 2
		}).addClass('animation-slidedown').show();
		$maskLayer.fadeTo(300, 0.5);

		//事件绑定
		$dialog.click(function(e) {
			var panelNode = this;
			if ($(e.target).is(".positive_btn")) {
				EventBus.trigger("nextStep");
			} else if ($(e.target).is(".close")) {
				me.close(panelNode);
			}
		});

		$(".check-invoice-header").click(function(e) {
			// console.log(e.target);
			var event = jQuery.Event("change.tabs", {
				target: $(e.target).attr("data-type"),
				context: this
			});
			EventBus.trigger(event);
		});
	};

	CheckInvoiceDialog.prototype.close = function(node) {
		$(node).fadeOut(300, function() {
			$(node).remove();
		}).removeClass('animation-slidedown');
		$maskLayer.fadeOut(300, function() {
			$(node).remove();
		});
	}

	//渲染结果
	CheckInvoiceDialog.prototype.renderResultList = function(data) {
		var tpl = require('../view/CheckInvoiceResult.html');
		me._populate(".check-result", data, tpl);
	};
	
	//渲染表单
	CheckInvoiceDialog.prototype.renderForm = function(data) {
		var myData = {},
			tpl = require('../view/CheckInvoiceForm.html');
		myData[data] = data;
		me._populate("#check-form", myData, tpl);
		$("#valid-form").on("submit", function(event) {
			event.preventDefault();
			if (!me._checkForm(this)) {
				var event = jQuery.Event("submit.check_invoice", {
					params: $(this).serialize(),
					context: this,
					record:{"orderId":$("[data-type='order-id']").val(),"num":$("[name='num']").val(),"useable":"useable"}
				});
				EventBus.trigger(event);
			}
		});
	};
	CheckInvoiceDialog.prototype.showAmount = function(amount) {
		var amountNode = $("p>span.band-amount");
		if (isNaN(amount)) {
			amountNode.closest("p").html(amount).addClass("danger");
		}else{
			amountNode.closest("p").removeClass("danger");
			amountNode.html(amount);
		}
	};
	CheckInvoiceDialog.prototype.showError = function(nodeId, msg) {
		var errorTips = $("#valid-form").find("p.alert-error");
		if (errorTips) {
			me.clearError(errorTips);
		};
		$('<p class="alert alert-error" style="display: block;">' + msg + '</p>').appendTo($(nodeId));
		return errorTips;
	};
	CheckInvoiceDialog.prototype.clearError = function(errorTips) {
		errorTips.remove();
	}
	CheckInvoiceDialog.prototype._checkForm = function(formNode) {
		var errorTag = false,
			errorTips = $(formNode).find("p.alert-error"),
			inputs = $(formNode).find("input");
		inputs.each(function(index) {
			var myNode = $(this),
				myVal = myNode.val(),
				mayValLen = $.trim(myVal).length;
			if (mayValLen === 0) {
				myNode.addClass("has-error");
				errorTag = true;
			} else if (myNode.hasClass("number-require") && (isNaN(myVal))) {
				myNode.addClass("has-error");
				me.showError(formNode, "所填项应为数字");
				errorTag = true;
			} else {
				myNode.removeClass("has-error");
			}
		});
		if (!errorTag) {
			me.clearError(errorTips);
		};
		return errorTag;
	};
	// 对模板渲染数据的封装
	CheckInvoiceDialog.prototype._populate = function(nodeId, data, tpl) {
		// console.log("render-data start---", data);
		var template = Handlebars.compile(tpl);
		// this.dialogTemplate = Handlebars.compile(dialogTpl);
		var html = template({
			"data": data
		});
		$(nodeId).html(html);
	}

});