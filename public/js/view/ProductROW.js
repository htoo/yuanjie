define(function(require, exports, module){
	var $ = require('jquery');
	var productRowTpl_exchange = require('./ProductRow_exchange.html');
	var productRowTpl_return = require('./ProductRow_return.html');
	var Handlebars = require('handlebars');
	var EventBus = require('../event/EventBus');
	var nodeId = "";
	
	
	function ProductRow(nodeId_exchange,nodeId_return){
		this.nodeId_exchange = nodeId_exchange;
		this.nodeId_return = nodeId_return;
		this.productRowTemplate_exchange = Handlebars.compile(productRowTpl_exchange);
		this.productRowTemplate_return = Handlebars.compile(productRowTpl_return);
	}

	/**
	 * 商品列表
	 * @param nodeId
	 * @param data
	 */
	
	ProductRow.prototype.showProductRow = function(data){
		var htmlStr_exchange = null;
		var htmlStr_return = null;
		var me = this;
		// console.log(data)
		htmlStr_exchange = this.productRowTemplate_exchange({
			data : data
		});
		htmlStr_return = this.productRowTemplate_return({
			data : data
		});

		$(this.nodeId_exchange).html(htmlStr_exchange); 
		$(this.nodeId_return).html(htmlStr_return); 
	};
	
	module.exports = ProductRow;
});