define(function(require, exports, module){
	var $ = require('jquery');
	// var orderRowTpl = require('./OrderRow.html');
	var VerifyStatusInfoTpl = require('./verifyStatusInfo.html');
	var Handlebars = require('handlebars');
	var EventBus = require('../event/EventBus');
	var nodeId = "";
	module.exports = VerifyStatusInfo;
	
	function VerifyStatusInfo(nodeId){
		this.nodeId = nodeId;
		this.VerifyStatusInfoTemplate = Handlebars.compile(VerifyStatusInfoTpl);
	}

	/**
	 * 订单列表
	 * @param nodeId
	 * @param data
	 */
	
	VerifyStatusInfo.prototype.showVerifyStatusInfo = function(data){
		var htmlStr = null;
		var me = this;
		// console.log(data)
		htmlStr = this.VerifyStatusInfoTemplate({
			data : data
		});

		$(this.nodeId).html(htmlStr); 
	};
	
	VerifyStatusInfo.prototype.bindPager = function(pager){
		$(this.nodeId).parent().parent().find(".pagination").html(pager.getHtml());
		pager.bindEvents();
	};
	
});