var should = require('chai').should();
var request = require('supertest');
var host = "http://localhost:3000";
var faker = require('faker');

describe('user', function() {

    describe('register', function() {
        it('register should success', function(done) {
            var body = {
                username: 'sim',
                password: '123',
                nickname: 'sim',
                gender: 'male',
                phone: '18382383428',
                address: 'some street',
                code: 'Z19FeUNkQX',
                number: 12
            };

            request(host)
                .post('/user')
                .send(body)
                .expect(200)
                .end(function(err, res) {
                    if (err) done(err);
                    delete res.body.user.__v;
                    delete res.body.user._id;
                    delete res.body.user.hashed_password;

                    delete body.code;
                    delete body.password;
                    delete res.body.user.avatar;

                    res.body.user.should.eql(body)
                    done()
                })
        });

        it('register with empty field', function(done) {
            var body = {

            };

            request(host)
                .post('/user')
                .send(body)
                .expect(403)
                .end(function(err, res) {
                    res.text.should.eql('{\"error\":\"填写的内容不能为空\"}');
                    done()
                })
        });

        it('register with error ring info', function(done) {
            var body = {
                username: 'sim1',
                password: '123',
                nickname: 'sim',
                gender: 'female', //error
                phone: '18382383428',
                address: 'some street',
                code: 'Z19FeU111NkQX', //error
                number: 12
            };

            request(host)
                .post('/user')
                .send(body)
                .expect(403)
                .end(function(err, res) {
                    res.text.should.eql('{\"error\":\"戒指信息错误，请检查 戒指号码、性别、配对密码 是否正确\"}');
                    done()
                })
        });

        it('register, but the ring is already registed', function(done) {
            var body = {
                username: faker.Name.firstName(),
                password: '123',
                nickname: 'sim',
                gender: 'male',
                phone: '18382383428',
                address: 'some street',
                code: 'WJXKeLNk7X',
                number: 5
            };

            var body2 = {
                username: faker.Name.firstName(),
                password: '123',
                nickname: 'sim',
                gender: 'male',
                phone: '18382383428',
                address: 'some street',
                code: 'WJXKeLNk7X',
                number: 5
            };

            request(host)
                .post('/user')
                .send(body)
                .expect(200)
                .end(function(err, res) {
                    if (err) done(err);

                    request(host)
                        .post('/user')
                        .send(body2)
                        .expect(403)
                        .end(function(err, res) {
                            res.text.should.eql('{\"error\":\"该戒指已被注册\"}');
                            done();
                        })
                })
        });
    });

    describe('login', function() {
        it('login', function(done) {
            var body = {
                username: 'tangmonk',
                password: '123',
                nickname: 'sim',
                gender: 'male',
                phone: '18382383428',
                address: 'some street',
                code: 'bkJxFe8VJ7Q',
                number: 17
            };

            request(host)
                .post('/user')
                .send(body)
                .expect(200)
                .end(function(err, res) {
                    if (err) done(err);
                    request(host)
                        .post('/user/login')
                        .send({
                            username: 'tangmonk',
                            password: '123'
                        })
                        .expect(200)
                        .end(function(err, res) {
                            if (err) done(err);

                            res.body.user.username.should.eql('tangmonk')
                            done()
                        })
                })
        });
    });
});

function login(cb) {
    var name = faker.Name.firstName();
    var body = {
        username: name,
        password: '123',
        nickname: 'sim',
        gender: 'male',
        phone: '18382383428',
        address: 'some street',
        code: 'ZJbbFlUVkXX',
        number: 35
    };

    request(host)
        .post('/user')
        .send(body)
        .end(function(err, res) {
            request(host)
                .post('/user/login')
                .send({
                    username: name,
                    password: '123'
                })
                .expect(200)
                .end(function(err, res) {
                    cb(err, res);
                })
        })
}

module.exports.login = login;