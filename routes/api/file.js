var shortId = require('shortid');
var async = require('async');
var express = require('express');
var router = express.Router();
var fs = require('fs');
var uploads_folder = require(root + '/config/config').imageUploads;
var userAuth = require('../web/user').userLoginValidate;

/**
 * [upload file to local, (public/uploads/images) BASE64]
 * @param  {[bsee64 string]} req.body.file
 * @return {[file name]}
 */
router.post('/', userAuth, function(req, res) {
    if (!req.body.file) return res.send(403, {
        error: 'need file'
    });

    upload(uploads_folder, req.body.file, function(err, name) {
        if (err) return res.send(403, err);

        return res.send({
            file: name
        });
    })
});

function upload(folder, base64, cb) {
    var name = shortId.generate();

    try {
        var decodeFile = new Buffer(base64, 'base64');
    } catch (err) {
        console.log(err);
    }

    async.each(['', 'mini', 'preview', 'thumb', 'thumb_center', 'large', 'better_thumb', 'better_thumb_center'], function(variant, c) {
        var path = variant === '' ? folder + '/' + name : folder + '/' + variant + '_' + name;
        fs.writeFile(path, decodeFile, function(err) {
            c(err);
        })
    }, function (err) {
        cb(err, name);
    });
}

module.exports.router = router;