var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var CommentSchema = new Schema({
    _creator: { type: ObjectId, ref: 'User', required: true },
    content: { type: String, required: true },
});

CommentSchema.plugin(require('mongoose-timestamp'));

var PostSchema = new Schema({
    _creator: { type: ObjectId, ref: 'User', required: true },
    content: { type: String, required: true },
    images: [String],
    comments: [ CommentSchema ]
});

PostSchema.plugin(require('mongoose-timestamp'));

module.exports.Comment = mongoose.model('Comment', CommentSchema);
module.exports.Post = mongoose.model('Post', PostSchema);