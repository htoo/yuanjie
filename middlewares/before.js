var express = require('express');
var swig = require('swig');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressSession = require('express-session');
var MongoStore = require('connect-mongo')(expressSession);
var mongoose = require('mongoose');
var moment = require('moment');
var expressValidator = require('express-validator');
var config = require(root + '/config/config');
var moment = require('moment');
var sessionValidate = require(root + '/middlewares/index').sessionValidate;

//connect mongoose
var db = mongoose.connect(config.db.uri);
db.connection.on('error', console.error.bind(console, 'mongodb connection error:'));

//configration session
var session = expressSession({
    secret: config.session_secret,
    cookie: {
        maxAge: new Date(moment().add(1000, 'days').utc())
    },
    resave: true,
    saveUninitialized: true,
    store: new MongoStore({
        mongoose_connection: mongoose.connection,
    })
});


//swig filters
swig.setFilter('timestamp', function (input) {
  return moment(input * 1000 ).format('hh:mm:ss');
});

module.exports = function(app) {

    // view engine setup
    app.use(render);
    app.use(redirect);

    app.engine('swig', swig.renderFile);

    app.set('view engine', 'swig');
    app.set('views', root + '/views');

    app.use(logger('dev'));
    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({limit: '50mb'}));
    app.use(cookieParser());
    app.use(express.static(path.join(root, 'public')));
    app.use(expressValidator());

    // session middleware
    app.use(session);
    app.use(sessionValidate);
};

//override express render
function render(req, res, next) {
    var _render = res.render;
    res.req = req;
    res.render = function(view, options, fn) {
        var self = this;
        //append user to render
        options = options || {};
        options.user = options.user || req.user;

        if ( self.req.headers['content-type'] && self.req.headers['content-type'].indexOf('application/json') > -1 ) {
            
            if (options.error) {
                res.send(403, options);
            } else {
                res.send(options);
            }
        }else {
            _render.call(res, view, options, fn);    
        }
    };

    next();
}

function redirect(req, res, next) {
    var _redirect = res.redirect;
    res.req = req;
    res.redirect = function(url, options) {
        var self = this;
        //append user to render
        options = options || {};

        if ( self.req.headers['content-type'] && self.req.headers['content-type'].indexOf('application/json') > -1 ) {
            if (options.error) {
                res.send(403, options);
            } else {
                res.send(options);
            }
        }else {
            _redirect.call(res, url);    
        }
    };

    next();
}

module.exports.session = session;