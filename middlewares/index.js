var User = require( root + '/model/userModel.js').User;

function sessionValidate (req, res, next) {
    if (req.session.user_id) {
        User.findOne({_id: req.session.user_id}, function (err, user) {
            req.user = user;
            return next();
        })
    }else {
        return next();
    }
}

module.exports.sessionValidate = sessionValidate;